package dbconnection;

import java.sql.*;

public class Conexion {
	private static Connection con = null;
	public static Connection getConnection() {
		if(con != null) {
			return con;
		}
		else {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				String url = "jdbc:mysql://localhost:3305/Moodle";
				String user = "root";
				String password = "";
				
				con = DriverManager.getConnection(url,user,password);
				System.out.println("Connection effective :");
				
			}
			
			catch(ClassNotFoundException cne) 
			{
				System.out.println("***Driver***");
				cne.printStackTrace();
			}
			
			catch(SQLException e)
			{
				System.out.println("***SQLException***");
				System.out.println(e);
			}
			
			return con;
		}
	}
}
