package model;

import java.sql.*;

public class contentModel {
	private int idContent; 
	private String nameContent;
	private String originalContent;
	private Time lengthContent;
	private String directorContent;
	private String countryContent;
	private Date yearContent;
	private String languageContent;
	private String studioContent;
	
	//Getter and setter
	
	public int getIdContent() {
		return idContent;
	}
	public void setIdContent(int idContent) {
		this.idContent = idContent;
	}
	
	
	public String getNameContent() {
		return nameContent;
	}
	public void setNameContent(String nameContent) {
		this.nameContent = nameContent;
	}
	
	
	public String getOriginalContent() {
		return originalContent;
	}
	public void setOriginalContent(String originalContent) {
		this.originalContent = originalContent;
	}
	
	
	public Time getLengthContent() {
		return lengthContent;
	}
	public void setLengthContent(Time lengthContent) {
		this.lengthContent = lengthContent;
	}
	
	
	public String getDirectorContent() {
		return directorContent;
	}
	public void setDirectorContent(String directorContent) {
		this.directorContent = directorContent;
	}
	
	
	public String getCountryContent() {
		return countryContent;
	}
	public void setCountryContent(String countryContent) {
		this.countryContent = countryContent;
	}
	
	
	public Date getYearContent() {
		return yearContent;
	}
	public void setYearContent(Date yearContent) {
		this.yearContent = yearContent;
	}
	
	
	public String getLanguageContent() {
		return languageContent;
	}
	public void setLanguageContent(String languageContent) {
		this.languageContent = languageContent;
	}
	
	
	public String getStudioContent() {
		return studioContent;
	}
	public void setStudioContent(String studioContent) {
		this.studioContent = studioContent;
	}
	
	
	

}
