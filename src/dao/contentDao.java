package dao;

import java.sql.*;
import java.time.Year;
import java.util.*;
import model.contentModel;
import dbconnection.Conexion;


public class contentDao {
		private Connection conn;
		public contentDao(){
			conn = Conexion.getConnection();
		}
		public void addContent(contentModel add) {
			try {
				String sql = "INSERT INTO content(idContent,nameContent,originalContent,lengthContent,directorContent,countryContent,yearContent,languageContent,studioContent)" + "VALUES (?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, add.getIdContent());
				ps.setString(2, add.getNameContent());
				ps.setString(3, add.getOriginalContent());
				ps.setTime(4, add.getLengthContent());
				ps.setString(5, add.getDirectorContent());
				ps.setString(6, add.getCountryContent());
				ps.setDate(7, add.getYearContent());
				ps.setString(8, add.getLanguageContent());
				ps.setString(9, add.getStudioContent());
				ps.executeUpdate();
			}
			catch(SQLException e){
				System.out.println("Insertar excepcion");
				e.printStackTrace();
				
			}
		}
		
		public void removeContent(int idContent) {
			try {
				String sql = "DELETE FROM content WHERE idContent=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1,idContent);
				ps.executeUpdate();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		public void editContent(contentModel edi) {
			try {
				String sql = "UPDATE content set nameContent=? ,originalContent=? ,lengthContent=? ,directorContent=? ,countryContent=? ,yearContent=? ,languageContent=? ,studioContent=? WHERE idContent=?;";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, edi.getIdContent());
				ps.setString(2, edi.getNameContent());
				ps.setString(3, edi.getOriginalContent());
				ps.setTime(4, edi.getLengthContent());
				ps.setString(5, edi.getDirectorContent());
				ps.setString(6, edi.getCountryContent());
				ps.setDate(7, edi.getYearContent());
				ps.setString(8, edi.getLanguageContent());
				ps.setString(9, edi.getStudioContent());
				ps.executeUpdate();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		public List getAllContent(){
			
			List liste_content = new ArrayList();
			try {
				
				String sql = "SELECT * FROM content";
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					contentModel list = new contentModel();
					list.setIdContent(rs.getInt("idContent"));
					list.setNameContent(rs.getString("nameContent"));
					list.setOriginalContent(rs.getString("originalNameContent"));
					list.setLengthContent(rs.getTime("lengthContent"));
					list.setDirectorContent(rs.getString("directorContent"));
					list.setCountryContent(rs.getString("countryContent"));
					list.setYearContent(rs.getDate("yearContent"));
					list.setLanguageContent(rs.getString("languageContent"));
					list.setStudioContent(rs.getString("studioContent"));
					liste_content.add(list);
				}
			}
			
			catch(SQLException e) {
				e.printStackTrace();
			}
			
			return liste_content;
		}
		
		
}
