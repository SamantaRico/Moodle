package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.contentDao;
import model.contentModel;


public class ContentCRUD extends HttpServlet {
	private static final String UserRecord = null;
	private static String Insert = "/newContent.jsp";
	private static String Edit ="/updateContent.jsp";
	private static String Delete = "/deleteContent.jsp";
	private static String Liste = "index.jsp";
	
	private contentDao dao;
	
	public ContentCRUD(){
		super();
		dao = new contentDao();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		String redirect = "";
		String id = request.getParameter("idContent");
		String action = request.getParameter("action");
		
		if( id != null && action.equalsIgnoreCase("insert")) {
			int idCont = Integer.parseInt(id);
			
			contentModel add = new contentModel();
			
			System.out.println("idContent : "+ idCont);
			System.out.println("nameContent : "+ request.getParameter("nameContent"));
			System.out.println("originalNameContent : "+ request.getParameter("originalNameContent"));
			System.out.println("lengthContent : "+ request.getParameter("lengthContent"));
			System.out.println("directorContent : "+ request.getParameter("directorContent"));
			System.out.println("countryContent : "+ request.getParameter("countryContent"));
			System.out.println("yearContent : "+ request.getParameter("yearContent"));
			System.out.println("languageContent : "+ request.getParameter("languageContent"));
			System.out.println("studioContent : "+ request.getParameter("studioContent"));
			
			add.setIdContent(idCont);
			add.setNameContent(request.getParameter("nameContent"));
			add.setOriginalContent(request.getParameter("originalNameContent"));
			add.setLengthContent(request.getParameter("lengthContent"));
			add.setDirectorContent(request.getParameter("directorContent"));
			add.setCountryContent(request.getParameter("countryContent"));
			add.setYearContent(request.getParameter("yearContent"));
			add.setLanguageContent(request.getParameter("languageContent"));
			add.setStudioContent(request.getParameter("studioContent"));
			
			dao.addContent(add);
			redirect = Liste;
			request.setAttribute("users", dao.getAllContent());
			System.out.println("Lista de Contenidos");
			
		}
		
		else if(action.equalsIgnoreCase("Delete")) {
			String idContent = request.getParameter("idContent");
			int idCon = Integer.parseInt(idContent);
			dao.removeContent(idCon);
			redirect = Liste;
			request.setAttribute("contents",dao.getAllContent());
			System.out.println("Eliminar Contenido");
		}
		
		else if(action.equalsIgnoreCase("EditForm")) {
			request.setAttribute("idContent", request.getParameter("idContent"));
			redirect = Edit;
		}
		else if(action.equalsIgnoreCase("Edit")) {
			String idContent = request.getParameter("idContent");
			int idCon = Integer.parseInt(idContent);
			contentModel content = new contentModel();
			content.setIdContent(idCon);
			content.setNameContent(request.getParameter("nameContent"));
			content.setOriginalContent(request.getParameter("originalNameContent"));
			content.setLengthContent(request.getParameter("lengthContent"));
			content.setDirectorContent(request.getParameter("directorContent"));
			content.setCountryContent(request.getParameter("countryContent"));
			content.setYearContent(request.getParameter("yearContent"));
			content.setLanguageContent(request.getParameter("languageContent"));
			content.setStudioContent(request.getParameter("studioContent"));
			
			dao.editContent(content);
			request.setAttribute("contents", content);
			redirect = Liste;
			System.out.println("Registro Modificado");
		}
		else if(action.equalsIgnoreCase("ListContent")) {
			request.setAttribute("contenidos", dao.getAllContent());
		}
		else { 
			redirect = Insert;
		}
		
		RequestDispatcher rd = request.getRequestDispatcher(redirect);
		rd.forward(request, response);	
	}
	
	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		doGet(request, response);
	}

}
