<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import ="model.contentModel" %>
<%@page import = "dao.contentDao" %>
<%@page import = "java.util.*" %>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Moodle</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"/>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	

</head>
<body>
	<%
	contentDao dao = new contentDao();
	List<contentModel> contentList = dao.getAllContent();
	Iterator<contentModel> itr = contentList.iterator();
	contentModel content = null;
	%>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class=navbar-brand" href="#">CRUD Moodle</a>
			</div>
		
		<div class="navbar-callapse collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.jsp">Mostrar</a>
				</li>
				<li><a href="ContentCRUD?action=insert">Nuevo</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdowm-toggle" data-toggle="dropdown">Men�</a>
					<ul class="dropdown-menu">
						<li><a href="#">Opci�n1</a>
						</li>
						<li><a href="#">Opci�n2</a>
						</li>
						<li><a href="#">Opci�n3</a>
						</li>
						<li><a class="divider"></li>
						<li><a href="#">Opci�n3</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		</div>
	</div>
	
	<div id="frm">
		<table class="table table-hover">
			<tr id="ca">
				<th>IdContenido</th>
				<th>NombreContenido</th>
				<th>NombreOriginal</th>
				<th>Duraci�nContenido</th>
				<th>DirectorContenido</th>
				<th>Pa�sContenido</th>
				<th>FechaContenido</th>
				<th>IdiomaContenido</th>
				<th>EstudioContenido</th>
				<th>Editar</th>
				<th>Eliminar</th>
			</tr>
			<tr>
				<%
					while(itr.hasNext()){
						content = itr.next();
				%>
				<td id="id"><%= content.getIdContent() %></td>
				<td id="name"><%= content.getNameContent() %></td>
				<td id="ori"><%= content.getOriginalContent()%></td>
				<td id="leng"><%= content.getLengthContent() %></td>
				<td id="dire"><%= content.getDirectorContent() %></td>
				<td id="coun"><%= content.getCountryContent() %></td>
				<td id="year"><%= content.getYearContent() %></td>
				<td id="langu"><%= content.getLanguageContent() %></td>
				<td id="stud"><%= content.getStudioContent() %></td>
				<td id="edi">
					<form action="ContentCRUD" method="POST">
						<button class="glyphicon glyphicon-pencil"></button>
						<input type="hidden" name="action" value="EditForm">
						<input type="hidden" name="idContent" value="content.getIdContent() %>" >
					</form>
				<td id="eli">
					<form method="POST" action="ContentCRUD">
						<button class="glyphicon glyphicon-trash"></button>
						<input type ="hidden" name="action" value="Delete">
						<input type ="hidden" name="idContent" value="<%= content.getIdContent()%>">
					</form>
				</td>
			</tr>
			<%
					}
			%>
		</table>
		<div id="new">
		<a class="glyphicon glyphicon-plus" href="ContentCRUD?action=insert">Nuevo</a>
		
		</div>
	
	</div>
</body>
</html>